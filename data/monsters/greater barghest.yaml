ability_mods:
  cha_mod: 5
  con_mod: 4
  dex_mod: 2
  int_mod: 3
  str_mod: 6
  wis_mod: 3
ac: 25
ac_special: null
alignment: CE
automatic_abilities:
- action_cost: Reaction
  critical_failure: null
  critical_success: null
  description: null
  effect: You lash out at a foe that leaves an opening. Make a melee Strike against
    the triggering creature. If your attack is a critical hit and the trigger was
    a manipulate action, you disrupt that action. This Strike doesn't count toward
    your multiple attack penalty, and your multiple attack penalty doesn't apply to
    this Strike.
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Attack of Opportunity
  range: null
  raw_description: '**Attack of Opportunity** [Reaction] **Trigger** A creature within
    your reach uses a manipulate action or a move action, makes a ranged attack, or
    leaves a square during a move action it''s using. **Effect** You lash out at a
    foe that leaves an opening. Make a melee Strike against the triggering creature.
    If your attack is a critical hit and the trigger was a manipulate action, you
    disrupt that action. This Strike doesn''t count toward your multiple attack penalty,
    and your multiple attack penalty doesn''t apply to this Strike.'
  requirements: null
  success: null
  traits: null
  trigger: A creature within your reach uses a manipulate action or a move action,
    makes a ranged attack, or leaves a square during a move action it's using.
description: 'Once a barghest has eaten enough to grow into a greater barghest, it
  typically seeks a method to leave the Material Plane and return to the Abyss, there
  joining other fiends as yet another of that plane''s horrors. As barghests have
  no innate ability to travel the planes, though, the time it takes for most greater
  barghests to engineer such a return can usually be measured in years, if not decades.
  During that time, greater barghests continue to play the roles of gluttons, hunters
  of humanity, and tyrants of goblinoid tribes. More than a few grow accustomed to
  such lives on the Material Plane and wholly abandon the end goal of returning to
  the Abyss, despite the fact that those who do make such a return home often grow
  even more powerful over time, gaining eerie new abilities and qualities absorbed
  from the raw chaos of the Abyss itself.




  In addition to greater barghests being more powerful than typical barghests, the
  process of transforming into a greater barghest results in hideous mutations that
  often grant deadly abilities. Some barghests grow large bat-like wings upon their
  transformation. Others develop toxic breath or vestigial limbs. The options detailed
  in the stats below represent only the tip of the proverbial iceberg for barghest
  mutations—feel free to use these as inspiration for coming up with new mutations
  of your own design.




  **__Recall Knowledge - Fiend__ (__Religion__)**: DC 25'
hp: 105
hp_misc: null
immunities: null
items: null
languages:
- Abyssal
- Common
- Goblin
level: 7
melee:
- action_cost: One Action
  damage:
    formula: 2d10+6
    type: piercing
  name: jaws
  plus_damage: null
  to_hit: 17
  traits: null
- action_cost: One Action
  damage:
    formula: 2d8+6
    type: slashing
  name: claw
  plus_damage: null
  to_hit: 17
  traits:
  - agile
name: Greater Barghest
perception: 16
proactive_abilities:
- action_cost: One Action
  critical_failure: null
  critical_success: null
  description: The barghest takes on the shape of a goblinoid (a goblin, hobgoblin,
    or bugbear) or a wolf, or it transforms back into its true form. When the barghest
    is a goblinoid, it loses its jaws and claw Strikes, it becomes Small if it is
    a goblin, and its Speed changes to 20 feet. When the barghest is a wolf, its Speed
    changes to 40 feet and its jaws gain Knockdown. Each individual barghest has only
    one goblinoid form and one wolf form.
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Change Shape
  range: null
  raw_description: '**Change Shape**   (__concentrate__, __divine__, __polymorph__,
    __transmutation__) The barghest takes on the shape of a goblinoid (a goblin, hobgoblin,
    or bugbear) or a wolf, or it transforms back into its true form. When the barghest
    is a goblinoid, it loses its jaws and claw Strikes, it becomes Small if it is
    a goblin, and its Speed changes to 20 feet. When the barghest is a wolf, its Speed
    changes to 40 feet and its jaws gain Knockdown. Each individual barghest has only
    one goblinoid form and one wolf form.'
  requirements: null
  success: null
  traits:
  - concentrate
  - divine
  - polymorph
  - transmutation
  trigger: null
- action_cost: None
  critical_failure: null
  critical_success: null
  description: The process of consuming corpses to evolve into a greater barghest
    results in odd and unpredictable physiological changes. A greater barghest has
    one mutation, typically chosen from the following options.
  effect: null
  effects:
  - action_cost: None
    critical_failure: null
    critical_success: null
    description: The barghest grows elongated fangs that seep poison. Its jaws deal
      1d6 additional poison damage and 1d6 persistent poison damage.
    effect: null
    effects: null
    failure: null
    frequency: null
    full_description: null
    generic_description: null
    name: Fangs
    range: null
    raw_description: '**Fangs** (__poison__) The barghest grows elongated fangs that
      seep poison. Its jaws deal 1d6 additional poison damage and 1d6 persistent poison
      damage.'
    requirements: null
    success: null
    traits:
    - poison
    trigger: null
  - action_cost: Two Actions
    critical_failure: null
    critical_success: null
    description: The barghest breathes a cloud of toxic gas that deals 8d6 poison
      damage to all creatures in a 30-foot cone (DC 25 basic Fortitude save). It can't
      use Toxic Breath again for 1d4 rounds.
    effect: null
    effects: null
    failure: null
    frequency: null
    full_description: null
    generic_description: null
    name: Toxic Breath
    range: null
    raw_description: '**Toxic Breath** [Two Actions] (__divine__, __evocation__, __poison__)
      The barghest breathes a cloud of toxic gas that deals 8d6 poison damage to all
      creatures in a 30-foot cone (DC 25 basic Fortitude save). It can''t use Toxic
      Breath again for 1d4 rounds.'
    requirements: null
    success: null
    traits:
    - divine
    - evocation
    - poison
    trigger: null
  - action_cost: Free Action
    critical_failure: null
    critical_success: null
    description: null
    effect: The barghest makes a claw Strike with a shriveled third arm hanging from
      its torso. This attack doesn't count for the barghest's multiple attack penalty,
      nor does that penalty apply on the attack.
    effects: null
    failure: null
    frequency: once per round
    full_description: null
    generic_description: null
    name: Vestigial Arm Strike
    range: null
    raw_description: '**Vestigial Arm Strike** [Free Action] **Frequency **once per
      round; **Trigger **The barghest completes a Strike. **Effect **The barghest
      makes a claw Strike with a shriveled third arm hanging from its torso. This
      attack doesn''t count for the barghest''s multiple attack penalty, nor does
      that penalty apply on the attack.'
    requirements: null
    success: null
    traits: null
    trigger: The barghest completes a Strike.
  - action_cost: None
    critical_failure: null
    critical_success: null
    description: The barghest has malformed wings extending from its back. It gains
      a fly Speed of 25 feet.
    effect: null
    effects: null
    failure: null
    frequency: null
    full_description: null
    generic_description: null
    name: Wings
    range: null
    raw_description: '**Wings** The barghest has malformed wings extending from its
      back. It gains a fly Speed of 25 feet.'
    requirements: null
    success: null
    traits: null
    trigger: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Mutations
  range: null
  raw_description: "**Mutations** The process of consuming corpses to evolve into\
    \ a greater barghest results in odd and unpredictable physiological changes. A\
    \ greater barghest has one mutation, typically chosen from the following options.\
    \ \n\n  * **Fangs** (__poison__) The barghest grows elongated fangs that seep\
    \ poison. Its jaws deal 1d6 additional poison damage and 1d6 persistent poison\
    \ damage.\n\n  * **Toxic Breath**  (__divine__, __evocation__, __poison__) The\
    \ barghest breathes a cloud of toxic gas that deals 8d6 poison damage to all creatures\
    \ in a 30-foot cone (DC 25 basic Fortitude save). It can't use Toxic Breath again\
    \ for 1d4 rounds.\n\n  * **Vestigial Arm Strike** [Free Action] **Frequency **once\
    \ per round; **Trigger **The barghest completes a Strike. **Effect **The barghest\
    \ makes a claw Strike with a shriveled third arm hanging from its torso. This\
    \ attack doesn't count for the barghest's multiple attack penalty, nor does that\
    \ penalty apply on the attack.\n\n  * **Wings** The barghest has malformed wings\
    \ extending from its back. It gains a fly Speed of 25 feet."
  requirements: null
  success: null
  traits: null
  trigger: null
ranged: null
rarity: Uncommon
resistances:
- amount: 10
  type: fire
- amount: 10
  type: physical (except magical)
ritual_lists: null
saves:
  fort: 17
  fort_misc: null
  misc: null
  ref: 15
  ref_misc: null
  will: 12
  will_misc: null
sense_abilities: null
senses:
- Perception +16
- darkvision
- scent (imprecise) 30 feet
size: Large
skills:
- bonus: 15
  misc: null
  name: 'Acrobatics '
- bonus: 15
  misc: null
  name: 'Athletics '
- bonus: 18
  misc: null
  name: 'Deception '
- bonus: 14
  misc: null
  name: 'Diplomacy '
- bonus: 16
  misc: null
  name: 'Intimidation '
- bonus: 15
  misc: null
  name: 'Stealth '
- bonus: 14
  misc: null
  name: 'Survival '
source:
- abbr: Bestiary
  page_start: 37
  page_stop: null
speed:
- amount: 35
  type: Land
spell_lists:
- dc: 25
  misc: null
  name: Divine Innate Spells
  spell_groups:
  - heightened_level: null
    level: 4
    spells:
    - frequency: at will
      name: blink
      requirement: null
    - frequency: null
      name: confusion
      requirement: null
    - frequency: null
      name: dimension door
      requirement: null
    - frequency: null
      name: enlarge
      requirement: null
  - heightened_level: null
    level: 3
    spells:
    - frequency: at will
      name: levitate
      requirement: null
  - heightened_level: null
    level: 2
    spells:
    - frequency: at will
      name: invisibility
      requirement: null
  - heightened_level: null
    level: 1
    spells:
    - frequency: null
      name: charm
      requirement: null
  to_hit: null
traits:
- Uncommon
- CE
- Large
- Fiend
- Mutant
type: Creature
weaknesses:
- amount: 5
  type: good
- amount: 5
  type: lawful
