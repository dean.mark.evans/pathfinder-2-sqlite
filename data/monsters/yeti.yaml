ability_mods:
  cha_mod: -1
  con_mod: 4
  dex_mod: 2
  int_mod: -1
  str_mod: 5
  wis_mod: 4
ac: 21
ac_special: null
alignment: N
automatic_abilities:
- action_cost: None
  critical_failure: null
  critical_success: null
  description: Yetis gain a +4 status bonus to saves against fear and against spells
    and abilities that affect dreams. A yeti that falls prey to a supernatural nightmare
    loses this ability and becomes permanently enraged, gaining a +1 status bonus
    to attack and damage rolls and a -1 status penalty to AC.
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Nightmare Guardian
  range: null
  raw_description: '**Nightmare Guardian** Yetis gain a +4 status bonus to saves against
    fear and against spells and abilities that affect dreams. A yeti that falls prey
    to a supernatural nightmare loses this ability and becomes permanently enraged,
    gaining a +1 status bonus to attack and damage rolls and a -1 status penalty to
    AC.'
  requirements: null
  success: null
  traits: null
  trigger: null
- action_cost: Reaction
  critical_failure: null
  critical_success: null
  description: null
  effect: The yeti Strides or Climbs up to half its Speed to a location where it can
    Hide, then Hides. If its new Stealth check result meets or exceeds the triggering
    creature's Perception DC, the yeti remains hidden.
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Vanish
  range: null
  raw_description: '**Vanish** [Reaction] **Trigger** The yeti is hidden or undetected
    while not in combat, and a creature would observe it. **Effect** The yeti Strides
    or Climbs up to half its Speed to a location where it can Hide, then Hides. If
    its new Stealth check result meets or exceeds the triggering creature''s Perception
    DC, the yeti remains hidden.'
  requirements: null
  success: null
  traits: null
  trigger: The yeti is hidden or undetected while not in combat, and a creature would
    observe it.
description: 'Nearly a myth, the yeti is rarely seen—and even when it is, it is often
  too late. Yetis dwell amid the highest, most remote peaks of the world, coming down
  from their snowy mountain holds to raid, steal livestock, and sometimes feed their
  insatiable urges for slaughter and destruction. Those folks who live at the foot
  of a yeti-ruled mountain warn of the "abominable snowmen": monstrous, fur-covered
  humanoids who leave strange and bloody tracks in the snow.




  Normally yetis seek to protect the world rather than hunt its other civilized denizens.
  They do so by lairing near eldritch portals that form links between the Material
  Plane and other, much stranger dimensions of reality. From within these snow-covered
  arches and ancient stone doorways, aliens, living nightmares, fiends, and worse
  can emerge through dimensional networks into the world. Yetis who guard these portals
  sometimes succumb to these horrors and are corrupted, taking on the bloodthirsty
  urges and horrific behaviors of the very monsters they strive to protect the world
  against. Yetis who manifest such violent tendencies are driven out of their clan
  and forced to wander the mountaintops alone, thus giving rise to the myth of the
  legendary abominable snowman. Forced to fend for themselves, these exiled yetis
  often fully embrace the corrupting elements that caused their exile in the first
  place, growing more powerful and more deadly as a result. The most vile of these
  yetis turn to cannibalism, not out of need, but out of a sheer joy at the terror
  they inflict upon their kin. Invariably such cannibalistic yetis attract the attention
  of wendigos, which makes a bad situation all the worse.




  **__Recall Knowledge - Humanoid__ (__Society__)**: DC 22'
hp: 115
hp_misc: null
immunities:
- cold
items: null
languages:
- Aklo
level: 5
melee:
- action_cost: One Action
  damage:
    formula: 2d10+5
    type: slashing
  name: claw
  plus_damage: null
  to_hit: 15
  traits: null
name: Yeti
perception: 15
proactive_abilities:
- action_cost: Free Action
  critical_failure: null
  critical_success: null
  description: null
  effect: Each enemy within 30 feet that witnesses the attack (including the target
    of the attack) must attempt a DC 23 Will save. On a failure, the creature is __frightened
    2__; on a critical failure, it's __frightened 4__.
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Grizzly Arrival
  range: null
  raw_description: '**Grizzly Arrival** [Free Action]  (__emotion__, __fear__, __mental__)
    **Trigger** The yeti hits a creature in the first round of combat and the yeti
    was __hidden__ from that creature at the start of combat. **Effect** Each enemy
    within 30 feet that witnesses the attack (including the target of the attack)
    must attempt a DC 23 Will save. On a failure, the creature is __frightened 2__;
    on a critical failure, it''s __frightened 4__.'
  requirements: null
  success: null
  traits:
  - emotion
  - fear
  - mental
  trigger: The yeti hits a creature in the first round of combat and the yeti was
    __hidden__ from that creature at the start of combat.
- action_cost: One Action
  critical_failure: null
  critical_success: null
  description: claw
  effect: The monster automatically deals that Strike's damage again to the enemy.
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Rend
  range: null
  raw_description: A Rend entry lists a Strike the monster has. **Requirements** The
    monster hit the same enemy with two consecutive Strikes of the listed type in
    the same round. **Effect** The monster automatically deals that Strike's damage
    again to the enemy.
  requirements: The monster hit the same enemy with two consecutive Strikes of the
    listed type in the same round.
  success: null
  traits: null
  trigger: null
ranged: null
rarity: Uncommon
resistances:
- amount: 10
  type: fire
ritual_lists: null
saves:
  fort: 15
  fort_misc: null
  misc: +4 status to all saves vs. fear and dreams
  ref: 11
  ref_misc: null
  will: 13
  will_misc: null
sense_abilities:
- action_cost: None
  critical_failure: null
  critical_success: null
  description: When Hiding, the yeti is concealed by any snowfall, even if it's not
    thick enough to make other creatures concealed.
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Snowblind
  range: null
  raw_description: '**Snowblind** When Hiding, the yeti is concealed by any snowfall,
    even if it''s not thick enough to make other creatures concealed.'
  requirements: null
  success: null
  traits: null
  trigger: null
senses:
- Perception +15
- darkvision
- scent (imprecise) 30 feet
size: Large
skills:
- bonus: 14
  misc: null
  name: 'Athletics '
- bonus: 12
  misc: +15 in snow
  name: 'Stealth '
- bonus: 11
  misc: null
  name: 'Survival '
source:
- abbr: Bestiary
  page_start: 338
  page_stop: null
speed:
- amount: 35
  type: Land
- amount: 20
  type: climb
spell_lists: null
traits:
- Uncommon
- N
- Large
- Humanoid
type: Creature
weaknesses: null
