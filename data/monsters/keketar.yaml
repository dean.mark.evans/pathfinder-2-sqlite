ability_mods:
  cha_mod: 7
  con_mod: 7
  dex_mod: 5
  int_mod: 5
  str_mod: 9
  wis_mod: 7
ac: 40
ac_special: null
alignment: CN
automatic_abilities:
- action_cost: None
  critical_failure: null
  critical_success: null
  description: 'A keketar''s vital organs shift and change shape and position constantly.
    Immediately after the keketar takes acid, electricity, or sonic damage, it gains
    the listed amount of resistance to that damage type. This lasts for 1 hour or
    until the next time the protean takes damage of one of the other types (in which
    case its resistance changes to match that type), whichever comes first.


    The keketar is immune to polymorph effects unless it is a willing target. If blinded
    or deafened, the keketar automatically recovers at the end of its next turn as
    new sensory organs grow to replace the compromised ones.'
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Protean Anatomy
  range: null
  raw_description: '**Protean Anatomy** (__divine__, __transmutation__) A keketar''s
    vital organs shift and change shape and position constantly. Immediately after
    the keketar takes acid, electricity, or sonic damage, it gains the listed amount
    of resistance to that damage type. This lasts for 1 hour or until the next time
    the protean takes damage of one of the other types (in which case its resistance
    changes to match that type), whichever comes first.


    The keketar is immune to polymorph effects unless it is a willing target. If blinded
    or deafened, the keketar automatically recovers at the end of its next turn as
    new sensory organs grow to replace the compromised ones.'
  requirements: null
  success: null
  traits:
  - divine
  - transmutation
  trigger: null
- action_cost: None
  critical_failure: null
  critical_success: null
  description: 30 feet. A creature using a teleportation ability within the aura or
    arriving in it via teleportation must succeed at a DC 38 Fortitude save or wink
    out of existence for 1d4 rounds before completing the teleport. The creature can't
    act, sense anything, or be targeted. On a successful save, the creature completes
    the teleport normally but is stunned 1. Keketars are immune to this effect.
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Spatial Riptide
  range: null
  raw_description: '**Spatial Riptide** (__aura__, __divine__, __transmutation__)
    30 feet. A creature using a teleportation ability within the aura or arriving
    in it via teleportation must succeed at a DC 38 Fortitude save or wink out of
    existence for 1d4 rounds before completing the teleport. The creature can''t act,
    sense anything, or be targeted. On a successful save, the creature completes the
    teleport normally but is stunned 1. Keketars are immune to this effect.'
  requirements: null
  success: null
  traits:
  - aura
  - divine
  - transmutation
  trigger: null
- action_cost: Reaction
  critical_failure: null
  critical_success: null
  description: null
  effect: You lash out at a foe that leaves an opening. Make a melee Strike against
    the triggering creature. If your attack is a critical hit and the trigger was
    a manipulate action, you disrupt that action. This Strike doesn't count toward
    your multiple attack penalty, and your multiple attack penalty doesn't apply to
    this Strike.
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Attack of Opportunity
  range: null
  raw_description: '**Attack of Opportunity** [Reaction] **Trigger** A creature within
    your reach uses a manipulate action or a move action, makes a ranged attack, or
    leaves a square during a move action it''s using. **Effect** You lash out at a
    foe that leaves an opening. Make a melee Strike against the triggering creature.
    If your attack is a critical hit and the trigger was a manipulate action, you
    disrupt that action. This Strike doesn''t count toward your multiple attack penalty,
    and your multiple attack penalty doesn''t apply to this Strike.'
  requirements: null
  success: null
  traits: null
  trigger: A creature within your reach uses a manipulate action or a move action,
    makes a ranged attack, or leaves a square during a move action it's using.
description: 'The ruling caste of the proteans, keketars orchestrate attacks against
  the bastions of law and adjudicate protean disputes confidently and capriciously.
  A keketar resembles a shimmering, serpentine creature with spines, claws, and a
  dragon-like head. A keketar''s actual appearance is in constant flux, but they generally
  stay about 18 feet long with a weight of around 1,500 pounds. While their physical
  forms can vary, two things remain constant: First, a keketar''s eyes are always
  a piercing shade of amber or violet. Second, the keketar''s mark of office—a crown
  of shifting symbols that hovers above its head—never changes. A keketar cannot remove
  its crown but can suppress it, although most are loath to do so and consider such
  an act one of cowardice or shame.




  Keketars fill a role in protean society of a sort of priesthood, operating as intermediaries
  between the other proteans and the Speakers of the Depths. All other proteans defer
  to keketars, treating them in a way similar to how citizens of a mortal city would
  treat respected nobles; even more powerful proteans defer to the will of the keketars.
  As with many religions, dogma and theology are prone to interpretation and change,
  and among the proteans the situation is perhaps even more pronounced. Whatever the
  nature of and desires held by the mysterious Speakers of the Depths, individual
  keketars may come to dramatically different conclusions as to their will and intent.
  To the proteans, though, this inherent dissonance is a strength rather than a weakness.




  **__Recall Knowledge - Monitor__ (__Religion__)**: DC 36'
hp: 290
hp_misc: fast healing 10
immunities: null
items: null
languages:
- Abyssal
- Celestial
- Protean
- telepathy 100 feet
- tongues
level: 17
melee:
- action_cost: One Action
  damage:
    formula: 3d10+15
    type: piercing
  name: jaws
  plus_damage:
  - formula: 1d6
    type: chaotic and warpwave strike
  to_hit: 33
  traits:
  - chaotic
  - magical
  - reach 10 feet
- action_cost: One Action
  damage:
    formula: 2d10+15
    type: slashing
  name: claw
  plus_damage:
  - formula: 1d6
    type: chaotic and warpwave strike
  to_hit: 33
  traits:
  - agile
  - chaotic
  - magical
  - reach 10 feet
- action_cost: One Action
  damage:
    formula: 2d10+15
    type: bludgeoning
  name: tail
  plus_damage:
  - formula: null
    type: Grab
  to_hit: 33
  traits:
  - reach 15 feet
name: Keketar
perception: 30
proactive_abilities:
- action_cost: One Action
  critical_failure: null
  critical_success: null
  description: The keketar can take the appearance of any Huge or smaller creature.
    This doesn't change its Speed or its attack and damage bonuses with its Strikes,
    but might change the damage type its Strikes deal.
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Change Shape
  range: null
  raw_description: '**Change Shape**   (__concentrate__, __divine__, __polymorph__,
    __transmutation__) The keketar can take the appearance of any Huge or smaller
    creature. This doesn''t change its Speed or its attack and damage bonuses with
    its Strikes, but might change the damage type its Strikes deal.'
  requirements: null
  success: null
  traits:
  - concentrate
  - divine
  - polymorph
  - transmutation
  trigger: null
- action_cost: One Action
  critical_failure: null
  critical_success: null
  description: 1d10+15 bludgeoning, DC 42
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Constrict
  range: null
  raw_description: '**__Constrict__**   1d10+15 bludgeoning, DC 42'
  requirements: null
  success: null
  traits: null
  trigger: null
- action_cost: None
  critical_failure: null
  critical_success: null
  description: When the keketar casts __hallucinatory terrain__, it infuses the illusion
    with quasi-real substance. Creatures that do not disbelieve the illusion treat
    structures and terrain created through the spell as though they were real, ascending
    illusory stairs, becoming trapped by illusory quicksand, and so on.
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Reshape Reality
  range: null
  raw_description: '**Reshape Reality** (__concentrate__, __divine__, __polymorph__,
    __transmutation__) When the keketar casts __hallucinatory terrain__, it infuses
    the illusion with quasi-real substance. Creatures that do not disbelieve the illusion
    treat structures and terrain created through the spell as though they were real,
    ascending illusory stairs, becoming trapped by illusory quicksand, and so on.'
  requirements: null
  success: null
  traits:
  - concentrate
  - divine
  - polymorph
  - transmutation
  trigger: null
- action_cost: None
  critical_failure: null
  critical_success: null
  description: A creature struck by a keketar's jaws or claw Strike must succeed at
    a DC 36 Fortitude save or be subject to a warpwave.
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Warpwave Strike
  range: null
  raw_description: '**Warpwave Strike** (__divine__, __polymorph__, __transmutation__)
    A creature struck by a keketar''s jaws or claw Strike must succeed at a DC 36
    Fortitude save or be subject to a warpwave.'
  requirements: null
  success: null
  traits:
  - divine
  - polymorph
  - transmutation
  trigger: null
ranged: null
rarity: Common
resistances:
- amount: 10
  type: precision
- amount: 25
  type: protean anatomy
ritual_lists: null
saves:
  fort: 30
  fort_misc: null
  misc: +1 status to all saves vs. magic
  ref: 28
  ref_misc: null
  will: 34
  will_misc: null
sense_abilities:
- action_cost: None
  critical_failure: null
  critical_success: null
  description: A keketar can anticipate the most likely presence of a creature through
    a supernatural insight into chaotic probabilities and chance. This grants it the
    ability to sense creatures within the listed range. A creature under the effects
    of __nondetection__ or that is otherwise shielded from divinations and predictions
    cannot be noticed via entropy sense.
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Entropy Sense
  range: null
  raw_description: '**Entropy Sense** (__divination__, __divine__, __prediction__)
    A keketar can anticipate the most likely presence of a creature through a supernatural
    insight into chaotic probabilities and chance. This grants it the ability to sense
    creatures within the listed range. A creature under the effects of __nondetection__
    or that is otherwise shielded from divinations and predictions cannot be noticed
    via entropy sense.'
  requirements: null
  success: null
  traits:
  - divination
  - divine
  - prediction
  trigger: null
senses:
- Perception +30
- entropy sense (imprecise) 60 feet
- darkvision
size: Large
skills:
- bonus: 26
  misc: null
  name: 'Acrobatics '
- bonus: 30
  misc: null
  name: 'Athletics '
- bonus: 33
  misc: null
  name: 'Deception '
- bonus: 35
  misc: null
  name: 'Diplomacy '
- bonus: 35
  misc: null
  name: 'Intimidation '
- bonus: 30
  misc: null
  name: 'Religion '
- bonus: 28
  misc: null
  name: 'Stealth '
source:
- abbr: Bestiary
  page_start: 269
  page_stop: null
speed:
- amount: 40
  type: Land
- amount: 50
  type: fly
- amount: 40
  type: swim
- amount: 128
  type: <a style="text-decoration:underline" href="Spells.aspx?ID="><i>freedom of
    movement</i></a>
spell_lists:
- dc: 42
  misc: ''
  name: Divine Innate Spells
  spell_groups:
  - heightened_level: null
    level: 9
    spells:
    - frequency: chaotic only
      name: divine wrath
      requirement: null
    - frequency: null
      name: prismatic sphere
      requirement: null
  - heightened_level: null
    level: 8
    spells:
    - frequency: null
      name: baleful polymorph
      requirement: null
    - frequency: null
      name: confusion
      requirement: null
  - heightened_level: null
    level: 7
    spells:
    - frequency: null
      name: disintegrate
      requirement: null
    - frequency: at will
      name: dispel magic
      requirement: null
    - frequency: at will
      name: shatter
      requirement: null
    - frequency: x3
      name: warp mind
      requirement: null
  - heightened_level: null
    level: 6
    spells:
    - frequency: at will, self only
      name: teleport
      requirement: null
  - heightened_level: null
    level: 5
    spells:
    - frequency: at will
      name: creation
      requirement: null
    - frequency: null
      name: dimension door
      requirement: null
    - frequency: x2, see reshape reality
      name: hallucinatory terrain
      requirement: null
  - heightened_level: null
    level: 4
    spells:
    - frequency: at will
      name: confusion
      requirement: null
    - frequency: at will
      name: dimension door
      requirement: null
  - heightened_level: null
    level: 2
    spells:
    - frequency: at will, lawful only
      name: detect alignment
      requirement: null
  - heightened_level: 5
    level: -1
    spells:
    - frequency: null
      name: tongues
      requirement: null
  - heightened_level: 4
    level: -1
    spells:
    - frequency: null
      name: freedom of movement
      requirement: null
  to_hit: 32
traits:
- CN
- Large
- Monitor
- Protean
type: Creature
weaknesses:
- amount: 15
  type: lawful
