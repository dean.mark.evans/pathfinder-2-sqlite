ability_mods:
  cha_mod: 7
  con_mod: 8
  dex_mod: 5
  int_mod: 5
  str_mod: 9
  wis_mod: 6
ac: 45
ac_special: null
alignment: CE
automatic_abilities:
- action_cost: None
  critical_failure: null
  critical_success: null
  description: 10 feet, 4d6 fire damage (DC 39 basic Reflex)
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Dragon Heat
  range: null
  raw_description: '**Dragon Heat** (__arcane__, __aura__, __evocation__, __fire__);
    10 feet, 4d6 fire damage (DC 39 basic Reflex)'
  requirements: null
  success: null
  traits:
  - arcane
  - aura
  - evocation
  - fire
  trigger: null
- action_cost: None
  critical_failure: The creature is frightened 4.
  critical_success: The creature is unaffected by the presence.
  description: 90 feet, DC 40
  effect: null
  effects: null
  failure: The creature is frightened 2.
  frequency: null
  full_description: null
  generic_description: A creature that first enters the area must attempt a Will save.
    Regardless of the result of the saving throw, the creature is temporarily immune
    to this monster's Frightful Presence for 1 minute.
  name: Frightful Presence
  range: null
  raw_description: '**Frightful Presence** 90 feet, DC 40 A creature that first enters
    the area must attempt a Will save. Regardless of the result of the saving throw,
    the creature is temporarily immune to this monster''s Frightful Presence for 1
    minute.

    Critical Success The creature is unaffected by the presence.

    Success The creature is frightened 1.

    Failure The creature is frightened 2.

    Critical Success The creature is frightened 4.'
  requirements: null
  success: The creature is frightened 1.
  traits:
  - aura
  - emotion
  - fear
  - mental
  trigger: null
- action_cost: Reaction
  critical_failure: null
  critical_success: null
  description: Jaws only.
  effect: You lash out at a foe that leaves an opening. Make a melee Strike against
    the triggering creature. If your attack is a critical hit and the trigger was
    a manipulate action, you disrupt that action. This Strike doesn't count toward
    your multiple attack penalty, and your multiple attack penalty doesn't apply to
    this Strike.
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Attack of Opportunity
  range: null
  raw_description: '**Attack of Opportunity** [Reaction] **Trigger** A creature within
    your reach uses a manipulate action or a move action, makes a ranged attack, or
    leaves a square during a move action it''s using. **Effect** You lash out at a
    foe that leaves an opening. Make a melee Strike against the triggering creature.
    If your attack is a critical hit and the trigger was a manipulate action, you
    disrupt that action. This Strike doesn''t count toward your multiple attack penalty,
    and your multiple attack penalty doesn''t apply to this Strike.'
  requirements: null
  success: null
  traits: null
  trigger: A creature within your reach uses a manipulate action or a move action,
    makes a ranged attack, or leaves a square during a move action it's using.
- action_cost: Reaction
  critical_failure: null
  critical_success: null
  description: null
  effect: The dragon makes all the choices to determine the targets, destination,
    or other effects of the spell, as though it were the caster.
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Redirect Fire
  range: null
  raw_description: '**Redirect Fire** [Reaction] (__abjuration__, __arcane__); Trigger
    A creature within 100 feet casts a fire spell, or a fire spell otherwise comes
    into effect from a source within 100 feet. **Effect** The dragon makes all the
    choices to determine the targets, destination, or other effects of the spell,
    as though it were the caster.'
  requirements: null
  success: null
  traits:
  - abjuration
  - arcane
  trigger: null
description: 'The largest and most powerful of the chromatic dragons, red dragons
  are a menace to civilizations everywhere, and their strength is rivaled only by
  their arrogance. Red dragons see themselves as regents and overlords of all dragonkind.
  With their crowns of crimson spikes and their command of blistering frame, this
  is no haughty boast in their eyes—only unquestioned fact.




  Red dragons don''t deign to speak with lesser creatures; they simply dominate and
  burn, enslaving weaker creatures to act as servants and to look after their lairs
  while the dragons slumber away. They take pleasure in dominating these creatures,
  and they demand tribute from their supplicants. Those who anger or disappoint end
  up eaten or reduced to ash. They command their enslaved minions to constantly build
  onto their lairs, carving out new tunnels and designing cunning traps that ensure
  the dragon''s security.




  Driven by their arrogance, red dragons are the most likely of the chromatic dragons
  to ravage settlements. They want the world to see them in their rightful place as
  powerful tyrants, and they delight in threatening all other creatures. They have
  no qualms about bullying, manipulating, or killing to complete their goals—or simply
  intimidating others through a public display of brutality and dominance.




  As legendary as the brutishness of red dragons is the magnificence of their hoards
  of gold. Their lairs are often situated in dangerous places, with volcanoes being
  a favorite spot, as they find them foreboding and the constant warmth is comfortable.
  No matter the locale, red dragons sleep on a litter of coins and other treasures,
  which they zealously guard. Sometimes, the dragon''s internal heat causes these
  precious metals to fuse together.




  **__Recall Knowledge - Dragon__ (__Arcana__)**: DC 41'
hp: 425
hp_misc: null
immunities:
- fire
- paralyzed
- sleep
items: null
languages:
- Abyssal
- Common
- Draconic
- Dwarven
- Jotun
- Orcish
level: 19
melee:
- action_cost: One Action
  damage:
    formula: 4d10+17
    type: piercing
  name: jaws
  plus_damage:
  - formula: 3d6
    type: fire
  to_hit: 37
  traits:
  - fire
  - magical
  - reach 20 feet
- action_cost: One Action
  damage:
    formula: 4d8+17
    type: slashing
  name: claw
  plus_damage: null
  to_hit: 37
  traits:
  - agile
  - magical
  - reach 15 feet
- action_cost: One Action
  damage:
    formula: 4d10+15
    type: slashing
  name: tail
  plus_damage: null
  to_hit: 35
  traits:
  - magical
  - reach 25 feet
- action_cost: One Action
  damage:
    formula: 3d8+15
    type: slashing
  name: wing
  plus_damage: null
  to_hit: 35
  traits:
  - agile
  - magical
  - reach 20 feet
name: Ancient Red Dragon
perception: 35
proactive_abilities:
- action_cost: Two Actions
  critical_failure: null
  critical_success: null
  description: The dragon breathes a blast of flame that deals 20d6 fire damage in
    a 60-foot cone (DC 42 basic Reflex save). It can't use Breath Weapon again for
    1d4 rounds.
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Breath Weapon
  range: null
  raw_description: '**Breath Weapon** [Two Actions]  (__arcane__, __evocation__, __fire__)
    The dragon breathes a blast of flame that deals 20d6 fire damage in a 60-foot
    cone (DC 42 basic Reflex save). It can''t use Breath Weapon again for 1d4 rounds.'
  requirements: null
  success: null
  traits:
  - arcane
  - evocation
  - fire
  trigger: null
- action_cost: Two Actions
  critical_failure: null
  critical_success: null
  description: The dragon makes two claw Strikes and one wing Strike in any order.
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Draconic Frenzy
  range: null
  raw_description: '**Draconic Frenzy** [Two Actions]  The dragon makes two claw Strikes
    and one wing Strike in any order.'
  requirements: null
  success: null
  traits: null
  trigger: null
- action_cost: None
  critical_failure: null
  critical_success: null
  description: The dragon recharges its Breath Weapon whenever it scores a critical
    hit with a Strike.
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Draconic Momentum
  range: null
  raw_description: '**Draconic Momentum** The dragon recharges its Breath Weapon whenever
    it scores a critical hit with a Strike.'
  requirements: null
  success: null
  traits: null
  trigger: null
- action_cost: One Action
  critical_failure: null
  critical_success: null
  description: The red dragon attempts to take control of a magical fire or a fire
    spell within 100 feet. If it succeeds at a counteract check (counteract level
    10, counteract modifier +32), the original caster loses control of the spell or
    magic fire, control is transferred to the dragon, and the dragon counts as having
    Sustained the Spell with this action (if applicable). The dragon can choose to
    end the spell instead of taking control, if it chooses.
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Manipulate Flames
  range: null
  raw_description: '**Manipulate Flames**   (__arcane__, __concentrate__, __transmutation__)
    The red dragon attempts to take control of a magical fire or a fire spell within
    100 feet. If it succeeds at a counteract check (counteract level 10, counteract
    modifier +32), the original caster loses control of the spell or magic fire, control
    is transferred to the dragon, and the dragon counts as having Sustained the Spell
    with this action (if applicable). The dragon can choose to end the spell instead
    of taking control, if it chooses.'
  requirements: null
  success: null
  traits:
  - arcane
  - concentrate
  - transmutation
  trigger: null
ranged: null
rarity: Uncommon
resistances:
- amount: 20
  type: cold
ritual_lists: null
saves:
  fort: 35
  fort_misc: null
  misc: +1 status to all saves vs. magic
  ref: 32
  ref_misc: null
  will: 35
  will_misc: null
sense_abilities:
- action_cost: None
  critical_failure: null
  critical_success: null
  description: Smoke doesn't impair a red dragon's vision; it ignores the concealed
    condition from smoke.
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Smoke Vision
  range: null
  raw_description: '**Smoke Vision** Smoke doesn''t impair a red dragon''s vision;
    it ignores the concealed condition from smoke.'
  requirements: null
  success: null
  traits: null
  trigger: null
senses:
- Perception +35
- darkvision
- scent (imprecise) 60 feet
- smoke vision
size: Huge
skills:
- bonus: 30
  misc: null
  name: 'Acrobatics '
- bonus: 35
  misc: null
  name: 'Arcana '
- bonus: 37
  misc: null
  name: 'Athletics '
- bonus: 35
  misc: null
  name: 'Deception '
- bonus: 35
  misc: null
  name: 'Diplomacy '
- bonus: 37
  misc: null
  name: 'Intimidation '
- bonus: 33
  misc: null
  name: 'Stealth '
source:
- abbr: Bestiary
  page_start: 112
  page_stop: null
speed:
- amount: 60
  type: Land
- amount: 180
  type: fly
spell_lists:
- dc: 42
  misc: null
  name: Arcane Innate Spells
  spell_groups:
  - heightened_level: null
    level: 8
    spells:
    - frequency: at will
      name: wall of fire
      requirement: null
  - heightened_level: null
    level: 4
    spells:
    - frequency: at will
      name: suggestion
      requirement: null
  - heightened_level: 9
    level: 0
    spells:
    - frequency: null
      name: detect magic
      requirement: null
    - frequency: null
      name: read aura
      requirement: null
  to_hit: null
traits:
- Uncommon
- CE
- Huge
- Dragon
- Fire
type: Creature
weaknesses: null
