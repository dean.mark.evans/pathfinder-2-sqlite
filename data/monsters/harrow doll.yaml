ability_mods:
  cha_mod: 5
  con_mod: 2
  dex_mod: 4
  int_mod: 0
  str_mod: 3
  wis_mod: 5
ac: 26
ac_special: null
alignment: N
automatic_abilities:
- action_cost: None
  critical_failure: null
  critical_success: null
  description: 'Whenever the harrow doll attempts a specific type of saving throw,
    it rolls twice and takes the higher result. The type of saving throw is determined
    by the suit that featured most prominently in the doll''s most recent harrow reading:
    Fortitude (Hammers or Shields), Reflex (Keys or Books), or Will (Stars or Crowns).
    If the suit of its latest harrow reading is unknown, roll 1d6 at the beginning
    of combat to randomly determine it.'
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Fortune's Favor
  range: null
  raw_description: '**Fortune''s Favor** (__fortune__) Whenever the harrow doll attempts
    a specific type of saving throw, it rolls twice and takes the higher result. The
    type of saving throw is determined by the suit that featured most prominently
    in the doll''s most recent harrow reading: Fortitude (Hammers or Shields), Reflex
    (Keys or Books), or Will (Stars or Crowns). If the suit of its latest harrow reading
    is unknown, roll 1d6 at the beginning of combat to randomly determine it.'
  requirements: null
  success: null
  traits:
  - fortune
  trigger: null
description: 'Harrow dolls are unique constructs that can make predictions about the
  future. The more elite circuses and traveling shows frequently feature a harrow
  doll, and many use the doll''s abilities to learn more about locals to better customize
  shows to their tastes—or to find easy marks for later cons. Upset patrons that try
  to harm a harrow doll or reclaim their payment quickly learn that the construct
  is more than capable of defending itself.




  **__Recall Knowledge - Construct__ (__Arcana__, __Crafting__)**: DC 26'
hp: 120
hp_misc: null
immunities:
- bleed
- death effects
- disease
- doomed
- drained
- fatigued
- healing
- mental
- necromancy
- nonlethal
- paralyzed
- poison
- sickened
- unconscious
items:
- metal harrow deck
languages:
- Common
level: 8
melee:
- action_cost: One Action
  damage:
    formula: 2d12+6
    type: bludgeoning
  name: fist
  plus_damage:
  - formula: null
    type: Grab
  to_hit: 17
  traits:
  - agile
  - magical
name: Harrow Doll
perception: 14
proactive_abilities:
- action_cost: None
  critical_failure: null
  critical_success: null
  description: 'A creature struck by one of the harrow doll''s cards must attempt
    a DC 25 Will save or be cursed with misfortune, which forces the creature to roll
    twice and take the lower result on its next roll of a specific type, determined
    by the card''s suit (roll 1d6 to randomly determine the suit). A creature can
    be cursed with only one effect from harrowing misfortune at a time, with a new
    curse overriding any previous curse. The curse ends after 1 minute or after the
    specified roll is made, whichever comes first. The suits and their effects are:
    Hammers (melee attack roll), Keys (Reflex save), Shields (Fortitude save), Books
    (skill check), Stars (Will save), and Crowns (spell attack roll).'
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Harrowing Misfortune
  range: null
  raw_description: '**Harrowing Misfortune** (__curse__, __misfortune__) A creature
    struck by one of the harrow doll''s cards must attempt a DC 25 Will save or be
    cursed with misfortune, which forces the creature to roll twice and take the lower
    result on its next roll of a specific type, determined by the card''s suit (roll
    1d6 to randomly determine the suit). A creature can be cursed with only one effect
    from harrowing misfortune at a time, with a new curse overriding any previous
    curse. The curse ends after 1 minute or after the specified roll is made, whichever
    comes first. The suits and their effects are: Hammers (melee attack roll), Keys
    (Reflex save), Shields (Fortitude save), Books (skill check), Stars (Will save),
    and Crowns (spell attack roll).'
  requirements: null
  success: null
  traits:
  - curse
  - misfortune
  trigger: null
ranged:
- action_cost: One Action
  damage:
    formula: 2d8+6
    type: slashing
  name: harrow card
  plus_damage:
  - formula: null
    type: harrowing misfortune
  to_hit: 18
  traits:
  - magical
  - range increment 60 feet
rarity: Uncommon
resistances:
- amount: 5
  type: physical (except adamantine)
ritual_lists: null
saves:
  fort: 16
  fort_misc: null
  misc: null
  ref: 16
  ref_misc: null
  will: 17
  will_misc: null
sense_abilities:
- action_cost: None
  critical_failure: null
  critical_success: null
  description: 'A harrow doll can cast __augury__ as part of a harrow reading, which
    takes the usual 10 minutes. When casting __locate__, the harrow doll doesn''t
    need to have previously observed a specific object to learn its direction, but
    instead can detail the direction only vaguely, using such phrases as "beside a
    weeping mound" or "beneath the lost sky." When the harrow doll casts __mind reading__,
    there is no effect if the target critically succeeds its save. Each time a harrow
    doll makes a harrow reading, it also changes which saving throw is affected by
    its fortune''s favor ability.


    '
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Uncanny Divination
  range: null
  raw_description: '**Uncanny Divination** A harrow doll can cast __augury__ as part
    of a harrow reading, which takes the usual 10 minutes. When casting __locate__,
    the harrow doll doesn''t need to have previously observed a specific object to
    learn its direction, but instead can detail the direction only vaguely, using
    such phrases as "beside a weeping mound" or "beneath the lost sky." When the harrow
    doll casts __mind reading__, there is no effect if the target critically succeeds
    its save. Each time a harrow doll makes a harrow reading, it also changes which
    saving throw is affected by its fortune''s favor ability.


    '
  requirements: null
  success: null
  traits: null
  trigger: null
senses:
- Perception +14
- darkvision
size: Large
skills:
- bonus: 16
  misc: null
  name: 'Fortune-Telling Lore '
source:
- abbr: 'Pathfinder #154: Siege of the Dinosaurs'
  page_start: 81
  page_stop: null
speed:
- amount: 25
  type: Land
spell_lists:
- dc: 27
  misc: null
  name: Arcane Innate Spells
  spell_groups:
  - heightened_level: null
    level: 3
    spells:
    - frequency: null
      name: locate
      requirement: null
    - frequency: null
      name: mind reading
      requirement: null
  - heightened_level: null
    level: 2
    spells:
    - frequency: at will
      name: augury
      requirement: null
  to_hit: null
traits:
- Uncommon
- N
- Large
- Construct
type: Creature
weaknesses: null
