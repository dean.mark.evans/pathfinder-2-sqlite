ability_mods:
  cha_mod: 7
  con_mod: 6
  dex_mod: 5
  int_mod: 5
  str_mod: 9
  wis_mod: 5
ac: 42
ac_special: null
alignment: NG
automatic_abilities:
- action_cost: None
  critical_failure: null
  critical_success: null
  description: The dragon gains 2 reactions at the start of each of their turns.
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Extra Reaction
  range: null
  raw_description: '**Extra Reaction** The dragon gains 2 reactions at the start of
    each of their turns.'
  requirements: null
  success: null
  traits: null
  trigger: null
- action_cost: None
  critical_failure: The creature is frightened 4.
  critical_success: The creature is unaffected by the presence.
  description: 90 feet, DC 37
  effect: null
  effects: null
  failure: The creature is frightened 2.
  frequency: null
  full_description: null
  generic_description: A creature that first enters the area must attempt a Will save.
    Regardless of the result of the saving throw, the creature is temporarily immune
    to this monster's Frightful Presence for 1 minute.
  name: Frightful Presence
  range: null
  raw_description: '**Frightful Presence** 90 feet, DC 37 A creature that first enters
    the area must attempt a Will save. Regardless of the result of the saving throw,
    the creature is temporarily immune to this monster''s Frightful Presence for 1
    minute.

    Critical Success The creature is unaffected by the presence.

    Success The creature is frightened 1.

    Failure The creature is frightened 2.

    Critical Success The creature is frightened 4.'
  requirements: null
  success: The creature is frightened 1.
  traits:
  - aura
  - emotion
  - fear
  - mental
  trigger: null
- action_cost: None
  critical_failure: null
  critical_success: null
  description: 30 feet. A swirling vortex of reflected color and light shimmers around
    the crystal dragon. Creatures in this aura's emanation are __dazzled__. Each creature
    that ends its turn in the emanation must succeed at a DC 34 Will saving throw
    or be __stunned 1__ (or stunned 3 on a critical failure). Once a creature succeeds
    at this save, it is temporarily immune to the stunning effect for 1 minute. The
    crystal dragon can turn this aura on or off using a single action, which has the
    __concentrate__ trait, and it can choose not to affect allies.
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Scintillating Aura
  range: null
  raw_description: '**Scintillating Aura** (__aura__, __evocation__, __incapacitation__,
    __primal__, __visual__) 30 feet. A swirling vortex of reflected color and light
    shimmers around the crystal dragon. Creatures in this aura''s emanation are __dazzled__.
    Each creature that ends its turn in the emanation must succeed at a DC 34 Will
    saving throw or be __stunned 1__ (or stunned 3 on a critical failure). Once a
    creature succeeds at this save, it is temporarily immune to the stunning effect
    for 1 minute. The crystal dragon can turn this aura on or off using a single action,
    which has the __concentrate__ trait, and it can choose not to affect allies.'
  requirements: null
  success: null
  traits:
  - aura
  - evocation
  - incapacitation
  - primal
  - visual
  trigger: null
- action_cost: Reaction
  critical_failure: null
  critical_success: null
  description: null
  effect: The crystal dragon adjusts a wing to try to reflect the spell and gains
    a +4 circumstance bonus to AC against the triggering attack. If the attack misses,
    the spell is reflected back at the caster, who must roll a second ranged spell
    attack roll against their own AC to determine if the spell hits them instead.
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Reflect Spell
  range: null
  raw_description: '**Reflect Spell [Reaction]** **Trigger **The crystal dragon is
    targeted by a ranged spell attack roll; **Effect **The crystal dragon adjusts
    a wing to try to reflect the spell and gains a +4 circumstance bonus to AC against
    the triggering attack. If the attack misses, the spell is reflected back at the
    caster, who must roll a second ranged spell attack roll against their own AC to
    determine if the spell hits them instead.'
  requirements: null
  success: null
  traits: null
  trigger: The crystal dragon is targeted by a ranged spell attack roll
- action_cost: Reaction
  critical_failure: null
  critical_success: null
  description: null
  effect: The dragon makes a tail Strike at the creature with a -2 penalty. If it
    hits, the dragon disrupts the creature's action.
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Twisting Tail
  range: null
  raw_description: '**Twisting Tail [Reaction]** **Trigger **A creature within reach
    of the dragon''s tail uses a __move__ action or leaves a square during a move
    action it''s using; **Effect **The dragon makes a tail Strike at the creature
    with a -2 penalty. If it hits, the dragon disrupts the creature''s action.'
  requirements: null
  success: null
  traits: null
  trigger: A creature within reach of the dragon's tail uses a __move__ action or
    leaves a square during a move action it's using
description: 'Good-natured but vain, crystal dragons are beautiful creatures with
  brilliant hides made of multicolored crystal and gemstone. Their beauty is a source
  of great pride but is also something of a weakness, as crystal dragons are easily
  angered by insults about their appearance. Despite their relatively benign natures
  when compared to other true dragons, crystal dragons can be short tempered and prone
  to finding insults where none were intended. Although their opinions are changeable,
  crystal dragons prefer orderly environments and are not fond of sudden interruptions
  or distractions.




  Crystal dragons build their lairs in underground grottoes, where they cultivate
  environments of great beauty. Their exacting standards and vivid imaginations mean
  that they are always working to improve the appearance or layout of some part of
  their lair. These lairs are unique to each individual crystal dragon, but there
  are always plenty of reflective surfaces that allow the dragon to observe their
  own appearance. These range from crystals to reflecting pools to finely crafted
  mirrors, which are arranged throughout the lair in a pleasing array. Gifts of well-crafted
  or magical mirrors are an excellent way to curry favor with a crystal dragon.




  Although crystal dragons are easily distracted by their sparkling collections or
  their vanity, they remain good-hearted creatures at the core and make friends quickly.
  While a crystal dragon tends to find evil creatures uncouth and unpleasant, any
  other nearby denizen or inhabitant of their home could become fast friends or beloved
  pets, depending upon the creature''s capacity for conversation (and for providing
  the frequent praise and compliments that crystal dragons hunger for).




  **__Recall Knowledge - Dragon__ (__Arcana__)**: DC 40


  **__Recall Knowledge - Elemental__ (__Arcana__, __Nature__)**: DC 40'
hp: 275
hp_misc: null
immunities:
- paralyzed
- sleep
items: null
languages:
- Celestial
- Common
- Draconic
- Terran
- Undercommon
level: 16
melee:
- action_cost: One Action
  damage:
    formula: 3d8+17
    type: slashing
  name: jaws
  plus_damage:
  - formula: 4d6
    type: piercing
  to_hit: 33
  traits:
  - magical
  - reach 20 feet
- action_cost: One Action
  damage:
    formula: 3d8+17
    type: slashing
  name: claw
  plus_damage: null
  to_hit: 33
  traits:
  - agile
  - magical
  - reach 15 feet
- action_cost: One Action
  damage:
    formula: 2d10+17
    type: slashing
  name: tail
  plus_damage: null
  to_hit: 31
  traits:
  - magical
  - reach 25 feet
name: Ancient Crystal Dragon
perception: 28
proactive_abilities:
- action_cost: Two Actions
  critical_failure: null
  critical_success: null
  description: The dragon breathes a flurry of piercing crystals that deals 17d6 piercing
    damage in a 50-foot cone (DC 38 basic Reflex save). They can't use Breath Weapon
    again for 1d4 rounds.
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Breath Weapon
  range: null
  raw_description: '**Breath Weapon** [Two Actions]  (__sonic__, __evocation__, __primal__)
    The dragon breathes a flurry of piercing crystals that deals 17d6 piercing damage
    in a 50-foot cone (DC 38 basic Reflex save). They can''t use Breath Weapon again
    for 1d4 rounds.'
  requirements: null
  success: null
  traits:
  - sonic
  - evocation
  - primal
  trigger: null
- action_cost: Free Action
  critical_failure: null
  critical_success: null
  description: null
  effect: "The dragon embeds transformative crystals in the creature's flesh. The\
    \ creature must attempt a DC 37 Fortitude save. \n\n"
  effects: null
  failure: null
  frequency: The crystal dragon damages a creature made of flesh with a jaws Strike
  full_description: null
  generic_description: null
  name: Crystallize Flesh
  range: null
  raw_description: "**Crystallize Flesh** [Free Action]  (__primal__, __transmutation__)\
    \ **Trigger **The crystal dragon damages a creature made of flesh with a jaws\
    \ Strike; **Frequency **three times per day; **Effect **The dragon embeds transformative\
    \ crystals in the creature's flesh. The creature must attempt a DC 37 Fortitude\
    \ save. \n\n**Critical Success **The target is unaffected. \n\n**Success **The\
    \ target is __slowed 1__ for 1 round as portions of its flesh turn crystalline.\
    \ \n\n**Failure **The target is slowed 1 and must attempt a Fortitude save at\
    \ the end of each of its turns; this ongoing save has the __incapacitation__ trait.\
    \ On a failed save, the slowed condition value increases by 1 (or by 2 on a critical\
    \ failure). A successful save reduces the slowed condition value by 1. A creature\
    \ unable to act due to the slowed condition from Crystallize Flesh is __petrified__\
    \ permanently, transforming into a crystalline statue. The effect ends if the\
    \ creature is petrified or the slowed condition is removed. \n\n**Critical Failure\
    \ **As failure, but the target is initially slowed 2."
  requirements: null
  success: null
  traits:
  - primal
  - transmutation
  trigger: three times per day
- action_cost: Two Actions
  critical_failure: null
  critical_success: null
  description: The dragon makes two claw Strikes and one tail Strike in any order.
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Draconic Frenzy
  range: null
  raw_description: '**Draconic Frenzy** [Two Actions]  The dragon makes two claw Strikes
    and one tail Strike in any order.'
  requirements: null
  success: null
  traits: null
  trigger: null
- action_cost: None
  critical_failure: null
  critical_success: null
  description: The dragon recharges their Breath Weapon whenever they critically hit
    with a Strike.
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Draconic Momentum
  range: null
  raw_description: '**Draconic Momentum** The dragon recharges their Breath Weapon
    whenever they critically hit with a Strike.'
  requirements: null
  success: null
  traits: null
  trigger: null
ranged: null
rarity: Rare
resistances:
- amount: 15
  type: sonic
ritual_lists: null
saves:
  fort: 30
  fort_misc: null
  misc: +1 status to all saves vs. magic
  ref: 27
  ref_misc: null
  will: 29
  will_misc: null
sense_abilities: null
senses:
- Perception +28
- darkvision
- scent (imprecise) 60 feet
- tremorsense (imprecise) 60 feet
size: Gargantuan
skills:
- bonus: 22
  misc: null
  name: 'Acrobatics '
- bonus: 33
  misc: null
  name: 'Athletics '
- bonus: 29
  misc: null
  name: 'Deception '
- bonus: 29
  misc: null
  name: 'Intimidation '
- bonus: 27
  misc: null
  name: 'Nature '
- bonus: 28
  misc: null
  name: 'Stealth '
- bonus: 26
  misc: null
  name: 'Survival '
source:
- abbr: Bestiary 2
  page_start: 92
  page_stop: null
speed:
- amount: 60
  type: Land
- amount: 40
  type: burrow
- amount: 140
  type: fly
spell_lists:
- dc: 37
  misc: null
  name: Primal Innate Spells
  spell_groups:
  - heightened_level: null
    level: 7
    spells:
    - frequency: null
      name: prismatic spray
      requirement: null
  - heightened_level: null
    level: 5
    spells:
    - frequency: at will
      name: color spray
      requirement: null
    - frequency: at will
      name: glitterdust
      requirement: null
    - frequency: at will
      name: hypnotic pattern
      requirement: null
  - heightened_level: 7
    level: 0
    spells:
    - frequency: null
      name: dancing lights
      requirement: null
  to_hit: null
traits:
- Rare
- NG
- Gargantuan
- Dragon
- Earth
- Elemental
type: Creature
weaknesses: null
